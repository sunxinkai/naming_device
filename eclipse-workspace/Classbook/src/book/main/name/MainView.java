package book.main.name;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.JTable;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

public class MainView extends JFrame {
	public static JTextField textField = new JTextField();
	
	public static JPasswordField passwordField = new JPasswordField(20);
	public MainView() {
		getContentPane().setFont(new Font("΢���ź�", Font.PLAIN, 14));
		setTitle("\u70B9\u540D\u7C3F");
		getContentPane().setLayout(null);
		
		JLabel label = new JLabel("\u751F\u6B7B\u4E00\u77AC\u95F4");
		label.setFont(new Font("����", Font.PLAIN, 16));
		label.setBounds(165, 23, 91, 24);
		getContentPane().add(label);
		
		JLabel label_1 = new JLabel("\u5C0F\u6307\u8F7B\u8F7B\u4E00\u70B9");
		label_1.setFont(new Font("����", Font.PLAIN, 16));
		label_1.setBounds(37, 95, 96, 32);
		getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("\u6A2F\u6A79\u7070\u98DE\u70DF\u706D");
		label_2.setFont(new Font("����", Font.PLAIN, 16));
		label_2.setBounds(300, 108, 104, 19);
		getContentPane().add(label_2);
		
		JButton button = new JButton("\u8FDB\u5165");
		button.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		button.setBounds(315, 199, 93, 23);
		getContentPane().add(button);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String username=textField.getText();
				//��ȡ����
				String password=new String(passwordField.getPassword());
				Management ren=new Management();
				Administrator user=ren.login(username, password);
				if(user==null){
					JOptionPane.showInternalMessageDialog(MainView.this, "�û��������������");
				}else{
					@SuppressWarnings("unused")
					UseView use=new UseView();
					MainView.this.dispose();
					use.setSize(400, 500);
					use.setVisible(true);
					use.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					use.setLocationRelativeTo(null);
				}
			}
		});
		
		JButton button_1 = new JButton("\u9000\u51FA");
		button_1.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		button_1.setBounds(315, 248, 93, 23);
		getContentPane().add(button_1);
		button_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
		
		JLabel label_3 = new JLabel("\u7528\u6237\u540D\uFF1A");
		label_3.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		label_3.setBounds(37, 203, 68, 15);
		getContentPane().add(label_3);
		
		
		textField.setBounds(115, 193, 141, 32);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		
		JLabel label_4 = new JLabel("\u5BC6   \u7801\uFF1A");
		label_4.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		label_4.setBounds(37, 256, 54, 15);
		getContentPane().add(label_4);
		
		
		passwordField.setBounds(115, 250, 141, 32);
		getContentPane().add(passwordField);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MainView use=new MainView();
		use.setTitle("������¼������");
		use.setSize(450, 400);
		use.setVisible(true);
		use.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		use.setLocationRelativeTo(null);
	}
}
