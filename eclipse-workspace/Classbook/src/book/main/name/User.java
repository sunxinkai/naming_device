package book.main.name;
public class User {
	private String ID;
	private String name;
	private String clss;
	public User() {
		
	}
	public User(String ID, String name, String clss) {
		super();
		this.ID = ID;
		this.name = name;
		this.clss = clss;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getClss() {
		return clss;
	}
	public void setClss(String clss) {
		this.clss = clss;
	}
	
	public String toString() {
		return this.ID+","+this.name+","+this.clss;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
