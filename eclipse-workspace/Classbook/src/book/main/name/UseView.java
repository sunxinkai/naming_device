package book.main.name;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

public class UseView extends JFrame{
	public UseView() {
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menu = new JMenu("\u7528\u6237\u7BA1\u7406");
		menuBar.add(menu);
		
		JMenuItem menuItem = new JMenuItem("添加用户");
		menu.add(menuItem);
		menuItem.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				AddView add =new AddView();
				add.setSize(400, 400);
				add.setVisible(true);
				add.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				add.setLocationRelativeTo(null);
			}
		});
		
		JMenuItem menuItem_1 = new JMenuItem("\u5220\u9664\u7528\u6237");
		menu.add(menuItem_1);
		menuItem_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				DeleteView delete=new DeleteView();
				delete.setSize(400, 400);
				delete.setVisible(true);
				delete.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				delete.setLocationRelativeTo(null);
			}
		});
		
		JMenuItem menuItem_2 = new JMenuItem("\u4FEE\u6539\u7528\u6237");
		menu.add(menuItem_2);
		menuItem_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				UsermodView delete=new UsermodView();
				delete.setSize(400, 400);
				delete.setVisible(true);
				delete.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				delete.setLocationRelativeTo(null);
			}
		});
		
		JSeparator separator = new JSeparator();
		menu.add(separator);
		
		JMenuItem menuItem_3 = new JMenuItem("\u9000\u51FA");
		menu.add(menuItem_3);
		menuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
		
		JMenu menu_1 = new JMenu("\u6743\u9650\u7BA1\u7406");
		menuBar.add(menu_1);
		
		JMenuItem menuItem_4 = new JMenuItem("\u6E05\u7A7A\u7528\u6237");
		menu_1.add(menuItem_4);
		menuItem_4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				DeleteAllView delete=new DeleteAllView();
				delete.setSize(400, 300);
				delete.setVisible(true);
				delete.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				delete.setLocationRelativeTo(null);
			}
		});
		
		JMenuItem menuItem_5 = new JMenuItem("\u70B9\u540D");
		menu_1.add(menuItem_5);
		menuItem_5.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				CallnameView add=new CallnameView();
				add.setSize(450, 400);
				add.setVisible(true);
				add.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				add.setLocationRelativeTo(null);
			}
		});
		
		JMenuItem menuItem_6 = new JMenuItem("\u4FEE\u6539\u5BC6\u7801");
		menu_1.add(menuItem_6);
		menuItem_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				PassView pass=new PassView();
				pass.setSize(400,350);
				pass.setVisible(true);
				pass.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				pass.setLocationRelativeTo(null);
			}
		});
		
		JMenuItem menuItem_7 = new JMenuItem("\u4E2A\u4EBA\u4FE1\u606F");
		menu_1.add(menuItem_7);
		getContentPane().setLayout(null);
		menuItem_7.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				PersonalView personalView=new PersonalView();
				personalView.setSize(400, 350);
				personalView.setVisible(true);
				personalView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				personalView.setLocationRelativeTo(null);
			}
		});
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UseView use=new UseView();
		use.setTitle("点名系统主界面");
		use.setSize(400, 500);
		use.setVisible(true);
		use.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		use.setLocationRelativeTo(null);
	}
}
